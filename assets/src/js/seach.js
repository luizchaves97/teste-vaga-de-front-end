$(document).ready(function() {
    // pegando dados do get
    var query = location.search.slice(1);
    if(query !== ''){
        var partes = query.split('&');
        var gets = {};
        partes.forEach(function (parte) {
            var chaveValor = parte.split('=');
            var chave = chaveValor[0];
            var valor = chaveValor[1];
            gets[chave] = valor;
        });
        var cont = 0;
        var condicao = '';
        var article = '';
        $.getJSON( "../../news.json", function( data ) {
            $('.article').empty();
            $.each( data, function( key, val) {
                if(gets.categoria.indexOf('all') > -1){
                    condicao = val.titulo.indexOf(gets.titulo) > -1;
                }else{
                    condicao = val.titulo.indexOf(gets.titulo) > -1 && val.categoria.indexOf(gets.categoria) > -1;
                }
                if(condicao){
                    article =
                        "<article id='"+ key +"'>" +
                            "<div class='col-md-5 col-sm-5 no-padding-left'>" +
                                "<a href='"+val.url+"'><div class='imagem' style='background-image: url("+val.imagem+");'></div></a>" +
                            "</div>" +
                            "<div class='col-md-7 col-sm-7 no-padding-right'>" +
                                "<div class='content'>" +
                                    "<a href='"+val.url+"'><h1>"+val.titulo+"</h1></a>" +
                                    "<h2>"+val.descricao+"</h2>" +
                                    "<time>"+val.hora+" - "+val.data+" </time>" +
                                "</div>" +
                            "</div>" +
                        "</article>" +
                        "<hr class='divisor'>";
                    $(article).appendTo(".news .article");
                    cont++;
                }
            });

            if(cont == 0){
                article = "<h4 class='text-center'>Nenhum resultado encontrado!</h4>";
                $('.news .article').append(article);
            }

            var resultSearch = cont + ' resultados para “'+gets.titulo+'”';
            $('.news p').empty();
            $('.news p').append(resultSearch);

        });
    }
});