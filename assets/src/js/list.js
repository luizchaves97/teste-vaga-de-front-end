$(document).ready(function() {
    var cont = 0;
    $.getJSON( "../../news.json", function( data ) {
        //listando as noticias
        $.each( data, function( key, val) {
            var article =
                "<article id='"+ key +"'>" +
                    "<div class='col-md-5 col-sm-5 no-padding-left'>" +
                        "<a href='"+val.url+"'><div class='imagem' style='background-image: url("+val.imagem+");'></div></a>" +
                    "</div>" +
                    "<div class='col-md-7 col-sm-7 no-padding-right'>" +
                        "<div class='content'>" +
                            "<a href='"+val.url+"'><h1>"+val.titulo+"</h1></a>" +
                            "<h2>"+val.descricao+"</h2>" +
                            "<time>"+val.hora+" - "+val.data+" </time>" +
                        "</div>" +
                    "</div>" +
                "</article>" +
                "<hr class='divisor'>";
            $(article).appendTo(".news .article");
            cont++;
        });

        //populando o select de  categoria
        var catNoticias = 0;
        var catFotos = 0;
        $.each( data, function( key, val) {
            if (val.categoria.indexOf('Foto') > -1){
                catFotos++;
            }else{
                catNoticias++;
            }
        });
        var select =
            "<select type='text' name='categoria' class='select-da' required>" +
                "<option value='all'>Todas</option>" +
                "<option value='Noticia'>Notícias ("+catNoticias+")</option>" +
                "<option value='Foto'>Fotos ("+catFotos+")</option>" +
            "</select>";
        $(select).appendTo("div.select").selectpicker();

        var resultSearch = cont + ' resultados encontrados!';
        $('.news p').append(resultSearch);
    });
});
