$(document).ready(function() {
    $(".select-da.order").change(function () {
          var value = $(".select-da.order option:selected").val();
          var cont = 0;
          if(value === "mais-recente"){
              cont = 0;
              $.getJSON( "../../news.json", function( data ) {
                  $('.article').empty();
                  $.each( data, function( key, val) {
                      var article =
                          "<article id='"+ key +"'>" +
                                "<div class='col-md-5 col-sm-5 no-padding-left'>" +
                                    "<a href='"+val.url+"'><div class='imagem' style='background-image: url("+val.imagem+");'></div></a>" +
                                "</div>" +
                                "<div class='col-md-7 col-sm-7 no-padding-right'>" +
                                    "<div class='content'>" +
                                        "<a href='"+val.url+"'><h1>"+val.titulo+"</h1></a>" +
                                        "<h2>"+val.descricao+"</h2>" +
                                        "<time>"+val.hora+" - "+val.data+" </time>" +
                                    "</div>" +
                                "</div>" +
                          "</article>" +
                          "<hr class='divisor'>";
                      $(article).appendTo(".news .article");
                      cont++;
                  });

                  var resultSearch = cont + ' resultados encontrados!';
                  $('.news p').empty();
                  $('.news p').append(resultSearch);
              });
          }else{
              cont = 0;
              $.getJSON( "../../news.json", function( data ) {
                  $('.article').empty();
                  data.reverse();
                  $.each( data, function( key, val) {
                      var article =
                          "<article id='"+ key +"'>" +
                                "<div class='col-md-5 col-sm-5 no-padding-left'>" +
                                    "<a href='"+val.url+"'><div class='imagem' style='background-image: url("+val.imagem+");'></div></a>" +
                                "</div>" +
                                "<div class='col-md-7 col-sm-7 no-padding-right'>" +
                                    "<div class='content'>" +
                                        "<a href='"+val.url+"'><h1>"+val.titulo+"</h1></a>" +
                                        "<h2>"+val.descricao+"</h2>" +
                                        "<time>"+val.hora+" - "+val.data+" </time>" +
                                    "</div>" +
                                "</div>" +
                          "</article>" +
                          "<hr class='divisor'>";
                      $(article).appendTo(".news .article");
                      cont++;
                  });

                  var resultSearch = cont + ' resultados encontrados!';
                  $('.news p').empty();
                  $('.news p').append(resultSearch);
              });
          }
    });
});